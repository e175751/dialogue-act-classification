
# 卒業研究テーマ(応答の対話行為推定)
## 実行コマンド
+ 研究室の***Argon***に接続するコマンド
```
ssh -v -4 xxxxxxx@chatan.ie.u-ryukyu.ac.jp -N -L xxxx:xx.xx.xx.xx:xxxx 
```

### Usage
+ 環境構築
```
singularity run --nv PreSerch2.sif 
```
+ モデルの訓練方法
```
cd work/model
python3 trains.py --expr model_name
```

+ モデルの評価方法
```
cd work/model
python3 evaluations.py --expr model_name
```

### Model
+ hirnn_utter_attention
  + 発話層 => 自己注意機構と位置エンコーダ
+ hirnn_context_attention
  + 対話層 => 自己注意機構と位置エンコーダ
+ hirnn_only
  + 先行研究
+ hiattention_only
  + 発話層 => 自己注意機構と位置エンコーダ
  + 対話層 => 自己注意機構と位置エンコーダ
+ hirnn_utter_rnn_attention
  + 発話層 => rnnと自己注意機構
+ hirnn_context_rnn_attention
  + 対話層 => rnnと自己注意機構
+ hirnn_context_attention_rnn
  + 対話層 => 自己注意機構とrnn
+ suggestion_model
  + 発話層 => 自己注意機構と位置エンコーダ
  + 対話層 => 自己注意機構とrnn


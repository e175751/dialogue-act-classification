FROM continuumio/anaconda3:2019.10

RUN apt-get update \
    && apt-get install -y locales \
    && locale-gen ja_JP.UTF-8 \
    && echo "export LANG=ja_JP.UTF-8" >> ~/.bashrc

RUN pip install --upgrade pip && \
    pip install pandas && \ 
    pip install Keras && \
    pip install tensorflow && \
    pip install mecab-python3 && \ 
    pip install matplotlib && \
    pip install keras-self-attention

EXPOSE 8080

ENTRYPOINT ["jupyter-lab", "--ip=0.0.0.0", "--port=8080", "--no-browser", "--allow-root", "--NotebookApp.token=''"]

CMD ["--notebook-dir=/work"]

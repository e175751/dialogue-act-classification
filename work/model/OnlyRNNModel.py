import torch
import torch.nn as nn
from torch import optim
import time
import math
import numpy as np
import torch.nn.functional as F
from torchsummary import summary
import torch.nn.utils.rnn as rnn
from BlockModel import OnlyRNNUtteraceEncoder, DenceEncoder, OnlyRNNContextEncoder, OnlyRNNDAEncoder

class OnlyRnnModel(nn.Module):

    def __init__(self, utt_vocab, da_vocab, config, device):
        super(OnlyRnnModel, self).__init__()
        self.utter_encoder = OnlyRNNUtteraceEncoder(len(utt_vocab.word2id), config['UTT_EMBED'], config['UTT_HIDDEN'])

        self.context_encoder = OnlyRNNContextEncoder(config['CON_EMBED'], config['CON_HIDDEN'])

        self.da_encoder = OnlyRNNDAEncoder(len(utt_vocab.word2id), config['DA_EMBED'], config['DA_HIDDEN'])

        self.de_encoder = DenceEncoder(config['DA_HIDDEN'] + config['CON_HIDDEN'], config['DA_EMBED'], len(da_vocab.word2id))

        self.cross_entropy_loss = nn.CrossEntropyLoss(size_average=True)

        self.device = device

    def forward(self,  X_utter, X_da, Y_da, mask, utter_hidden, context_hidden, da_hidden, turn):
        
        utter_output, utter_hidden = self.utter_encoder(X_utter, utter_hidden)

        turn_output = torch.cat((utter_output, turn), dim=2)

        context_output, context_hidden = self.context_encoder(turn_output, context_hidden)

        da_output, da_hidden = self.da_encoder(X_da, da_hidden)

        x_output = torch.cat((context_output, da_output), dim=2)

        dence_output = self.de_encoder(x_output)

        output = dence_output.squeeze(1)  # (batch_size, da_dim)
        
        Y_da = Y_da.squeeze()
        
        loss = self.cross_entropy_loss(output, Y_da)

        loss.backward(retain_graph=True)

        return loss.item(), utter_hidden, context_hidden, da_hidden 

    
    def evaluate(self, X_utter, X_da, Y_da, mask, utter_hidden, context_hidden, da_hidden, turn):
        with torch.no_grad():

            utter_output, utter_hidden = self.utter_encoder(X_utter, utter_hidden)

            turn_output = torch.cat((utter_output, turn), dim=2)

            context_output, context_hidden = self.context_encoder(turn_output, context_hidden)

            da_output, da_hidden = self.da_encoder(X_da, da_hidden)

            x_output = torch.cat((context_output, da_output), dim=2)

            dence_output = self.de_encoder(x_output)

            output = dence_output.squeeze(1)  # (batch_size, da_dim)
            
            Y_da = Y_da.squeeze(0)
            
            loss = self.cross_entropy_loss(output, Y_da)

        return loss.item(), utter_hidden, context_hidden, da_hidden 


    def prediction(self, X_utter, X_da, mask, utter_hidden, context_hidden, da_hidden, turn):
        with torch.no_grad():

            utter_output, utter_hidden = self.utter_encoder(X_utter, utter_hidden)

            turn_output = torch.cat((utter_output, turn), dim=2)

            context_output, context_hidden = self.context_encoder(turn_output, context_hidden)

            da_output, da_hidden = self.da_encoder(X_da, da_hidden)

            x_output = torch.cat((context_output, da_output), dim=2)

            dence_output = self.de_encoder(x_output)

            output = dence_output.squeeze(1)

        return loss.item(), utter_hidden, context_hidden, da_hidden 

    
    def initDAHidden(self, batch_size):
        return self.utter_encoder.initHidden(batch_size, self.device), self.context_encoder.initHidden(batch_size, self.device), self.da_encoder.initHidden(batch_size, self.device)


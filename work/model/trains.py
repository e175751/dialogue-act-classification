import time
import os
import pyhocon
import torch
from torch import nn
from torch import optim
from models import *
from utils import *
from nn_blocks import *
import argparse
from torchsummary import summary
import random
import matplotlib.pyplot as plt
from torch.utils.tensorboard import SummaryWriter
import numpy as np
from HiAttentionModel import HiAttentionOnlyModel,  RNNDAPredictionModel, HiRnnUtterAttentionModel, HiRnnContextAttentionModel
from OnlyRNNModel import OnlyRnnModel
from AttentionRnnModel import UtteraceRnnAttentionModel, ContextRnnAttentionModel, ContextAwareRnnAttentionModel, SuggestionModel


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('--expr', '-e', default='DAonly', help='input experiment config')
    parser.add_argument('--gpu', '-g', type=int, default=0, help='input gpu num')
    args = parser.parse_args()
    if torch.cuda.is_available():
        device = torch.device('cuda:{}'.format(args.gpu))
    else:
        device = 'cpu'

    print('Use device: ', device)
    return args, device

def initialize_env(name):

    config = pyhocon.ConfigFactory.parse_file('experiments.conf')[name]
    config['log_dir'] = os.path.join(config['log_root'], name)
    if not os.path.exists(config['log_dir']):
        os.makedirs(config['log_dir'])

    return config

def create_DAdata(config):
    posts, cmnts, _, _, turn = create_traindata(config)
    X_train, Y_train, X_valid, Y_valid, X_test, Y_test, Tturn, Vturn, Testturn = separate_data(posts, cmnts, turn)
    return X_train, Y_train, X_valid, Y_valid, X_test, Y_test, Tturn, Vturn, Testturn

def create_Uttdata(config):
    _, _, posts, cmnts, turn = create_traindata(config)
    X_train, Y_train, X_valid, Y_valid, X_test, Y_test, _, _, _ = separate_data(posts, cmnts, turn)
    return X_train, Y_train, X_valid, Y_valid, X_test, Y_test

def select_model(mode_name, utt_vocab, da_vocab, config, device, lr):

    if mode_name == "hirnn_utter_attention":
        da_predict_model = HiRnnUtterAttentionModel(utt_vocab, da_vocab, config, device).to(device)
        da_predict_model_opt = optim.Adam(da_predict_model.parameters(), lr)

    elif mode_name == "hirnn_context_attention":
        da_predict_model = HiRnnContextAttentionModel(utt_vocab, da_vocab, config, device).to(device)
        da_predict_model_opt = optim.Adam(da_predict_model.parameters(), lr)

    elif mode_name == "hirnn_only":
        da_predict_model = OnlyRnnModel(utt_vocab, da_vocab, config, device).to(device)
        da_predict_model_opt = optim.Adam(da_predict_model.parameters(), lr)

    elif mode_name == "hiattention_only":
        da_predict_model = HiAttentionOnlyModel(utt_vocab, da_vocab, config, device).to(device)
        da_predict_model_opt = optim.Adam(da_predict_model.parameters(), lr)        

    elif mode_name == "hirnn_utter_rnn_attention":
        da_predict_model = UtteraceRnnAttentionModel(utt_vocab, da_vocab, config, device).to(device)
        da_predict_model_opt = optim.Adam(da_predict_model.parameters(), lr) 

    elif mode_name == "hirnn_context_rnn_attention":
        da_predict_model = ContextRnnAttentionModel(utt_vocab, da_vocab, config, device).to(device)
        da_predict_model_opt = optim.Adam(da_predict_model.parameters(), lr)

    elif mode_name == "hirnn_context_attention_rnn":
        da_predict_model = ContextAwareRnnAttentionModel(utt_vocab, da_vocab, config, device).to(device)
        da_predict_model_opt = optim.Adam(da_predict_model.parameters(), lr)

    elif mode_name == "suggestion_model":
        da_predict_model = SuggestionModel(utt_vocab, da_vocab, config, device).to(device)
        da_predict_model_opt = optim.Adam(da_predict_model.parameters(), lr)

    else:
        pass

    return da_predict_model, da_predict_model_opt


def train(experiment, device):

    wirter = SummaryWriter(log_dir="./logs")

    print('loading setting "{}"...'.format(experiment))
    config = initialize_env(experiment)

    X_train, Y_train, X_valid, Y_valid, _, _, Tturn, Vturn, _ = create_DAdata(config)
    XU_train, YU_train, XU_valid, YU_valid, _, _ = create_Uttdata(config)
    print('Finish create train data...')

    da_vocab = da_Vocab(config, X_train + X_valid, Y_train + Y_valid)
    utt_vocab = utt_Vocab(config, XU_train + XU_valid, YU_train + YU_valid)
    print('Finish create vocab dic...')


    X_train, Y_train = da_vocab.tokenize(X_train, Y_train)
    X_valid, Y_valid = da_vocab.tokenize(X_valid, Y_valid)
    XU_train, YU_train = utt_vocab.tokenize(XU_train, YU_train)
    XU_valid, YU_valid = utt_vocab.tokenize(XU_valid, YU_valid)

    print('Finish preparing dataset...')

    assert len(XU_train) == len(YU_train), 'Unexpect content in train data'
    assert len(XU_valid) == len(YU_valid), 'Unexpect content in valid data'

    lr = config['lr']
    batch_size = config['BATCH_SIZE']
    plot_train_losses = []
    plot_valid_losses = []
    
    print_total_loss = 0
    plot_total_loss = 0
    plot_total_acc = 0

    models, optims = select_model(experiment, utt_vocab, da_vocab, config, device, lr)    

    print('---start training---')

    start = time.time()
    k = 0
    _valid_loss = None
    correct = None

    for e in range(config['EPOCH']):

        tmp_time = time.time()
        print('Epoch {} start'.format(e+1))

        # TODO: 同じターン数でバッチ生成
        indexes = [i for i in range(len(X_train))]
        random.shuffle(indexes)
        k = 0

        while k < len(indexes):
            # initialize
            step_size = min(batch_size, len(indexes) - k)

            batch_idx = indexes[k : k + step_size]
            optims.zero_grad()
            utter_hidden, context_hidden, da_hidden = models.initDAHidden(step_size)

             #  create batch data
            print('\rConversation {}/{} training...'.format(k + step_size, len(X_train)), end='')
            X_seq = [X_train[seq_idx] for seq_idx in batch_idx]
            Y_seq = [Y_train[seq_idx] for seq_idx in batch_idx]
            turn_seq = [Tturn[seq_idx] for seq_idx in batch_idx]
            max_conv_len = max(len(s) for s in X_seq)  # seq_len は DA と UTT で共通

            if config['use_utt'] or config['use_uttcontext']:
                XU_seq = [XU_train[seq_idx] for seq_idx in batch_idx]
                YU_seq = [YU_train[seq_idx] for seq_idx in batch_idx]

                # conversation turn padding
                for ci in range(len(XU_seq)):
                    XU_seq[ci] = XU_seq[ci] + [[utt_vocab.word2id['<ConvPAD>']]] * (max_conv_len - len(XU_seq[ci]))
                    YU_seq[ci] = YU_seq[ci] + [[utt_vocab.word2id['<ConvPAD>']]] * (max_conv_len - len(YU_seq[ci]))

            for ci in range(len(X_seq)):
                X_seq[ci] = X_seq[ci] + [da_vocab.word2id['<PAD>']] * (max_conv_len - len(X_seq[ci]))
                Y_seq[ci] = Y_seq[ci] + [da_vocab.word2id['<PAD>']] * (max_conv_len - len(Y_seq[ci]))
                turn_seq[ci] = turn_seq[ci] + [0] * (max_conv_len - len(turn_seq[ci]))

            assert len(X_seq) == len(Y_seq), 'Unexpect sequence length'

            for i in range(0, max_conv_len):
                
                last = True if i == max_conv_len - 1 else False

                X_tensor = torch.tensor([[X[i]] for X in X_seq]).to(device)
                Y_tensor = torch.tensor([[Y[i]] for Y in Y_seq]).to(device)
                if config['turn']:
                    turn_tensor = torch.tensor([[t[i]] for t in turn_seq]).to(device)
                    turn_tensor = turn_tensor.float()
                    turn_tensor = turn_tensor.unsqueeze(1)    
                else:
                    turn_tensor = None
                if config['use_utt'] or config['use_uttcontext']:
                    max_seq_len = max(len(XU[i]) + 1 for XU in XU_seq)
                    # utterance padding
                    for ci in range(len(XU_seq)):
                        XU_seq[ci][i] = XU_seq[ci][i] + [utt_vocab.word2id['<UttPAD>']] * (max_seq_len - len(XU_seq[ci][i]))
                        YU_seq[ci][i] = YU_seq[ci][i] + [utt_vocab.word2id['<UttPAD>']] * (max_seq_len - len(YU_seq[ci][i]))
                    XU_tensor = torch.tensor([XU[i] for XU in XU_seq]).to(device)
                    YU_tensor = None
                else:
                    XU_tensor, YU_tensor = None, None
                
                loss, utter_hidden, context_hidden, da_hidden = models(XU_tensor, X_tensor, Y_tensor, None, utter_hidden, context_hidden, da_hidden, turn_tensor)
                print_total_loss += loss
                plot_total_loss += loss
                
                if last:
                    optims.step()

            k += step_size

        valid_loss = validation(X_valid, Y_valid, XU_valid, YU_valid, models, device, config, Vturn)

        wirter.add_scalars(experiment + "/loss/wid=5", {"Train_Loss": plot_total_loss,
                                             "Valid_Loss": valid_loss}, e)

        def save_model(filename):
            torch.save(models.state_dict(), os.path.join(config['log_dir'], config['SAVE_NAME'] + "_" + str(config['window_size']) + '.model'.format(filename)))


        if _valid_loss is None:
            save_model("_validbest")
            print("save model")
            _valid_loss = valid_loss
        else:
            if _valid_loss > valid_loss:
                save_model("_validbest")
                print("save model")
                _valid_loss = valid_loss

        print("steps %d\tloss %.4f\tvalid loss %.4f | exec time %.4f" % (e+1, print_total_loss, valid_loss, time.time()-tmp_time))
        plot_train_losses.append(plot_total_loss)
        plot_valid_losses.append(valid_loss)
        print_total_loss = 0
        plot_total_loss = 0

    wirter.close()
    print()
    print('Finish training | exec time: %.4f [sec]' % (time.time() - start))


def validation(X_valid, Y_valid, XU_valid, YU_valid, model, device, config, turn):

    total_loss = 0
    k = 0

    for seq_idx in range(len(X_valid)):
        print('\r{}/{} conversation evaluating'.format(seq_idx+1, len(X_valid)), end='')
        utter_hidden, context_hidden, da_hidden = model.initDAHidden(1)
        X_seq = X_valid[seq_idx]
        Y_seq = Y_valid[seq_idx]
        turn_seq = turn[seq_idx]
        
        if config['use_utt'] or config['use_uttcontext']:
            XU_seq = XU_valid[seq_idx]
            YU_seq = YU_valid[seq_idx]

        assert len(X_seq) == len(Y_seq), 'Unexpect sequence len in evaluate {} != {}'.format(len(X_seq), len(Y_seq))
        
        for i in range(0, len(X_seq)):
            X_tensor = torch.tensor([[X_seq[i]]]).to(device)
            Y_tensor = torch.tensor([[Y_seq[i]]]).to(device)
            turn_tensor = torch.tensor([[turn_seq[i]]]).to(device)
            turn_tensor = turn_tensor.float()
            turn_tensor = turn_tensor.unsqueeze(1)   
            XU_tensor = torch.tensor([XU_seq[i]]).to(device)
            
            loss, utter_hidden, context_hidden, da_hidden = model.evaluate(XU_tensor, X_tensor, Y_tensor, None, utter_hidden, context_hidden, da_hidden, turn_tensor)
            total_loss += loss

    return total_loss


if __name__ == '__main__':
    args, device = parse()
    train(args.expr, device)
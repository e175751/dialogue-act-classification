import time
import os
import pyhocon
import torch
from torch import optim
from models import *
from nn_blocks import *
from utils import *
from train import initialize_env, create_DAdata, create_Uttdata, device
import argparse
from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix, accuracy_score
from sklearn.metrics import precision_recall_fscore_support as score
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from collections import Counter
from pprint import pprint
import numpy as np
import pickle
from HiAttentionModel import HiAttentionOnlyModel,  RNNDAPredictionModel, HiRnnUtterAttentionModel, HiRnnContextAttentionModel
from OnlyRNNModel import OnlyRnnModel
from AttentionRnnModel import UtteraceRnnAttentionModel, ContextRnnAttentionModel, ContextAwareRnnAttentionModel, SuggestionModel


parser = argparse.ArgumentParser()
parser.add_argument('--expr', default='DAonly')
parser.add_argument('--gpu', '-g', type=int, default=0, help='input gpu num')
args = parser.parse_args()

def select_model(mode_name, utt_vocab, da_vocab, config, device):

    if mode_name == "hirnn_utter_attention":
        da_predict_model = HiRnnUtterAttentionModel(utt_vocab, da_vocab, config, device).to(device)
        
    elif mode_name == "hirnn_context_attention":
        da_predict_model = HiRnnContextAttentionModel(utt_vocab, da_vocab, config, device).to(device)

    elif mode_name == "hirnn_only":
        da_predict_model = OnlyRnnModel(utt_vocab, da_vocab, config, device).to(device)
        
    elif mode_name == "hiattention_only":
        da_predict_model = HiAttentionOnlyModel(utt_vocab, da_vocab, config, device).to(device)
        
    elif mode_name == "hirnn_utter_rnn_attention":
        da_predict_model = UtteraceRnnAttentionModel(utt_vocab, da_vocab, config, device).to(device)

    elif mode_name == "hirnn_context_rnn_attention":
        da_predict_model = ContextRnnAttentionModel(utt_vocab, da_vocab, config, device).to(device)

    elif mode_name == "hirnn_context_attention_rnn":
        da_predict_model = ContextAwareRnnAttentionModel(utt_vocab, da_vocab, config, device).to(device)

    elif mode_name == "suggestion_model":
        da_predict_model = SuggestionModel(utt_vocab, da_vocab, config, device).to(device)

    else:
        pass

    return da_predict_model


def evaluations(experiment):
    print('load vocab')

    config = initialize_env(experiment)
    X_train, Y_train, X_valid, Y_valid, X_test, Y_test, _, _, turn = create_DAdata(config)
    da_vocab = da_Vocab(config, X_train + X_valid, Y_train + Y_valid)
    XU_train, YU_train, XU_valid, YU_valid, XU_test, YU_test = create_Uttdata(config)
    utt_vocab = utt_Vocab(config, XU_train + XU_valid, YU_train + YU_valid)

    X_test, Y_test = da_vocab.tokenize(X_test, Y_test)
    XU_test, _ = utt_vocab.tokenize(XU_test, YU_test)

    print('load model')

    da_predict_model = select_model(experiment, utt_vocab, da_vocab, config, device)
    da_predict_model.load_state_dict(torch.load(os.path.join(config['log_dir'], config['SAVE_NAME'] + "_" + str(config['window_size']) + '.model')))
    
    result = []

    for seq_idx in range(0, len(X_test)):
        print('\r{}/{} conversation evaluating'.format(seq_idx+1, len(X_test)), end='')
        X_seq = X_test[seq_idx]
        Y_seq = Y_test[seq_idx]
        turn_seq = turn[seq_idx]
        XU_seq = XU_test[seq_idx]
        assert len(X_seq) == len(Y_seq)

        pred_seq = []
        true_seq = []
        utter_hidden, context_hidden, da_hidden = da_predict_model.initDAHidden(1)
       
        for i in range(0, len(X_seq)):
            X_tensor = torch.tensor([[X_seq[i]]]).to(device)
            Y_tensor = torch.tensor(Y_seq[i]).to(device)
            turn_tensor = torch.tensor([[turn_seq[i]]]).to(device)
            turn_tensor = turn_tensor.float()
            turn_tensor = turn_tensor.unsqueeze(1)
            XU_tensor = torch.tensor([XU_seq[i]]).to(device)

            output, utter_hidden, context_hidden, da_hidden = da_predict_model.prediction(XU_tensor, X_tensor, None, utter_hidden, context_hidden, da_hidden, turn_tensor)

            pred_idx = torch.argmax(output)
            pred_seq.append(pred_idx.item())
            true_seq.append(Y_tensor.item())
            
            
        result.append({'true': true_seq,
                       'true_detok': [da_vocab.id2word[token] for token in true_seq],
                       'pred': pred_seq,
                       'pred_detok': [da_vocab.id2word[token] for token in pred_seq],
                       'UttSeq': [[utt_vocab.id2word[word] for word in sentence] for sentence in XU_seq],
                       'seq_detok': [da_vocab.id2word[label] for label in X_seq]})

    print()
    
    return result, da_vocab

def visualize_attention_weights(att_weights, seq_idx, result_string):
    plt.figure(figsize = (15, 7))
    sns.heatmap(att_weights.to('cpu').detach().numpy().copy(), fmt=".2f", cmap='Blues')
    plt.savefig('./data/visualize/att_weight_' + result_string + str(seq_idx) + '.png')
    plt.close()


def calc_average(y_true, y_pred):
    p = precision_score(y_true=y_true, y_pred=y_pred, average='macro')
    r = recall_score(y_true=y_true, y_pred=y_pred, average='macro')
    f = f1_score(y_true=y_true, y_pred=y_pred, average='macro')
    acc = accuracy_score(y_true=y_true, y_pred=y_pred)
    print('p: {} | r: {} | f: {} | acc: {}'.format(p, r, f, acc))


def save_cmx(y_true, y_pred, expr):
    fontsize = 40
    labels = [da_vocab.id2word[idx] for idx in sorted(list(set(y_true)))]
    y_true_list = [da_vocab.id2word[idx] for idx in y_true]
    y_pred_list = [da_vocab.id2word[idx] for idx in y_pred]
    
    cmx_data = confusion_matrix(y_true_list, y_pred_list, labels=labels, normalize='all')

    df_cmx = pd.DataFrame(cmx_data, index=labels, columns=labels)

    plt.figure(figsize=(40, 30))
    plt.rcParams['font.size'] = fontsize
    heatmap = sns.heatmap(df_cmx, annot=True, fmt='.4f', cmap='Blues')
    heatmap.yaxis.set_ticklabels(heatmap.yaxis.get_ticklabels(), rotation=0, ha='right', fontsize=fontsize)
    heatmap.xaxis.set_ticklabels(heatmap.xaxis.get_ticklabels(), rotation=45, ha='right', fontsize=fontsize)
    plt.xlabel('Predicted Label')
    plt.ylabel('True Label')
    plt.tight_layout()
    plt.savefig('./data/images/cmx_{}.png'.format(expr))

if __name__ == '__main__':

    result, da_vocab = evaluations(args.expr)

    true = [label for line in result for label in line['true']]
    pred = [label for line in result for label in line['pred']]

    calc_average(y_true=true, y_pred=pred)
    f = f1_score(y_true=true, y_pred=pred, average=None)
    [print(da_vocab.id2word[idx], score) for idx, score in zip(sorted(set(true)),f)]

    save_cmx(true, pred, expr=args.expr)

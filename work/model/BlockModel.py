import torch
import torch.nn as nn
from torch import optim
import time
import math
import numpy as np
import torch.nn.functional as F
from torchsummary import summary
import torch.nn.utils.rnn as rnn
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence



class UtteraceEncoder(nn.Module):

    def __init__(self, vocab_size, emb_dim, d_model):
        super(UtteraceEncoder, self).__init__()
        self.d_model = d_model
        self.embedding = WordEmbedding(vocab_size, emb_dim, self.d_model)
        self.encoding = PositinalEncoding(self.d_model, 200)
        self.attention = Attention(self.d_model)
        self.ffn = FeedForward(d_model, emb_dim)

    def forward(self, x_utter, mask):

        emb_output = self.embedding(x_utter)

        pos_output = self.encoding(emb_output)

        att_output, att_weights = self.attention(pos_output, pos_output, pos_output, mask)

        ffn_output = self.ffn(att_output)

        seq_len = ffn_output.size()[1]

        avg_output = F.avg_pool2d(ffn_output, (seq_len, 1)) # => (128, 1, 512)

        return avg_output, att_weights  # 発話ベクトル(128, 1, 512)

    def initHidden(self, batch_size, device):
        return torch.zeros(1, batch_size, self.d_model).to(device)


class ContextEncoder(nn.Module):

    def __init__(self, emb_dim, d_model):
        super(ContextEncoder, self).__init__()
        self.d_model = d_model
        self.emb_dim = emb_dim
        self.linear = nn.Linear(self.d_model+1, self.d_model)
        self.encoding = PositinalEncoding(self.d_model, 200)
        self.attention = Attention(self.d_model)
        self.ffn = FeedForward(self.d_model, self.d_model)

    def forward(self, x_hidden, mask, hidden):

        lin_output = self.linear(x_hidden)

        turn_output = torch.cat((lin_output, hidden), dim=1)
        
        # pos_output = self.encoding(turn_output)

        att_output, att_weights = self.attention(turn_output, turn_output, turn_output, mask)

        ffn_output = self.ffn(att_output)

        seq_len = ffn_output.size()[1]

        avg_output = F.avg_pool2d(ffn_output, (seq_len, 1))

        return avg_output, hidden, att_weights
        
    def initHidden(self, batch_size, device):
        return torch.zeros(batch_size, 0, self.d_model).to(device)


class DAEncoder(nn.Module):

    def __init__(self, da_size, emb_dim, d_model):
        super(DAEncoder, self).__init__() 
        self.d_model = d_model
        self.embedding = DAEmbedding(da_size, emb_dim, self.d_model)
        self.encoding = PositinalEncoding(self.d_model, 200)
        self.attention = Attention(self.d_model)
        self.ffn = FeedForward(d_model, emb_dim)

    def forward(self, X_da, mask, hidden):

        emb_output = self.embedding(X_da)

        hidden_output = torch.cat((hidden, emb_output), dim=1) # => (128, dailogue_len, 128)

        # pos_output = self.encoding(hidden_output)

        att_output, att_weights = self.attention(hidden_output, hidden_output, hidden_output, mask)

        ffn_output = self.ffn(att_output)

        seq_len = ffn_output.size()[1]

        avg_output = F.avg_pool2d(ffn_output, (seq_len, 1))

        return avg_output, hidden, att_weights
        
    def initHidden(self, batch_size, device):
        return torch.zeros(batch_size, 0, self.d_model).to(device)


class DenceEncoder(nn.Module):
    def __init__(self, da_hidden, da_embed_size, da_input_size):
        super(DenceEncoder, self).__init__()
        self.he = nn.Linear(da_hidden, da_embed_size)
        self.ey = nn.Linear(da_embed_size, da_input_size)

    def forward(self, hidden):
        pred = self.ey(torch.tanh(self.he(hidden)))
        return pred


class OnlyRNNUtteraceEncoder(nn.Module):

    def __init__(self, vocab_size, emb_dim, d_model):
        super(OnlyRNNUtteraceEncoder, self).__init__()
        self.d_model = d_model
        self.embedding = WordEmbedding(vocab_size, emb_dim, self.d_model)
        self.rnn = nn.GRU(self.d_model, self.d_model, batch_first=True)
        self.ffn = FeedForward(self.d_model, self.d_model)

    def forward(self, x_utter, hidden):

        emb_output = self.embedding(x_utter)
        
        rnn_output, rnn_hidden = self.rnn(emb_output, hidden)

        seq_len = rnn_output.size()[1]

        avg_output = F.avg_pool2d(rnn_output, (seq_len, 1))
        
        ffn_output = self.ffn(avg_output)

        return ffn_output, rnn_hidden

    def initHidden(self, batch_size, device):
        return torch.zeros(1, batch_size, self.d_model).to(device)


class OnlyRNNContextEncoder(nn.Module):
    def __init__(self, emb_dim, d_model):
        super(OnlyRNNContextEncoder, self).__init__()
        self.emb_dim = emb_dim
        self.d_model = d_model
        self.linear = nn.Linear(self.d_model+1, self.d_model)
        self.rnn = nn.GRU(self.d_model, self.d_model, batch_first=True)
        self.ffn = FeedForward(self.d_model, self.d_model)
    
    def forward(self, x, hidden):

        lin_output = self.linear(x)

        rnn_output, rnn_hidden = self.rnn(lin_output, hidden)

        ffn_output = self.ffn(rnn_output)
    
        return ffn_output, rnn_hidden
        
    def initHidden(self, batch_size, device):
        return torch.zeros(1, batch_size, self.d_model).to(device)


class OnlyRNNDAEncoder(nn.Module):
    
    def __init__(self, da_size, emb_dim, d_model):
        super(OnlyRNNDAEncoder, self).__init__()
        self.d_model = d_model
        self.embedding = DAEmbedding(da_size, emb_dim, self.d_model)
        self.rnn = nn.GRU(self.d_model, self.d_model, batch_first=True)
        self.ffn = FeedForward(self.d_model, emb_dim)    

    def forward(self, X_da, hidden):

        emb_output = self.embedding(X_da)
        
        rnn_output, rnn_hidden = self.rnn(emb_output, hidden)

        ffn_output = self.ffn(rnn_output)

        return ffn_output, rnn_hidden

    def initHidden(self, batch_size, device):
        return torch.zeros(1, batch_size, self.d_model).to(device)


class RNNUtterEncoder(nn.Module):

    def __init__(self, vocab_size, emb_dim, d_model):
        super(RNNUtterEncoder, self).__init__()
        self.d_model = d_model
        self.embedding = WordEmbedding(vocab_size, emb_dim, self.d_model)
        self.rnn = nn.GRU(self.d_model, self.d_model, batch_first=True)
        self.attention = Attention(self.d_model)
        self.ffn = FeedForward(self.d_model, emb_dim)

    def forward(self, x_utter, mask, hidden):

        emb_output = self.embedding(x_utter)
        
        rnn_output, rnn_hidden = self.rnn(emb_output, hidden)

        att_output, att_weights = self.attention(rnn_output, rnn_output, rnn_output, mask)

        ffn_output = self.ffn(att_output)

        seq_len = ffn_output.size()[1]

        avg_output = F.avg_pool2d(ffn_output, (seq_len, 1))

        return avg_output, att_weights, rnn_hidden

    def initHidden(self, batch_size, device):
        return torch.zeros(1, batch_size, self.d_model).to(device)


class RNNContextEncoder(nn.Module):

    def __init__(self, emb_dim, d_model):
        super(RNNContextEncoder, self).__init__()
        self.emb_dim = emb_dim
        self.d_model = d_model
        self.linear = nn.Linear(self.d_model+1, self.d_model)
        self.rnn = nn.GRU(self.d_model, self.d_model, batch_first=True)
        self.attention = Attention(self.d_model)
        self.ffn = FeedForward(self.d_model, self.d_model)

    def forward(self, x, mask, hidden):

        lin_output = self.linear(x)

        _, rnn_hidden = self.rnn(lin_output, hidden)
        
        att_output, att_weights = self.attention(rnn_hidden.transpose(0, 1), rnn_hidden.transpose(0, 1), rnn_hidden.transpose(0, 1), mask)

        ffn_output = self.ffn(att_output)
    
        return ffn_output, att_weights, rnn_hidden
        
    def initHidden(self, batch_size, device):
        return torch.zeros(1, batch_size, self.d_model).to(device)


class RNNContextAwareEncoder(nn.Module):
    
    def __init__(self, emb_dim, d_model):
        super(RNNContextAwareEncoder, self).__init__()
        self.d_model = d_model
        self.linear = nn.Linear(self.d_model+1, self.d_model)
        self.rnn = nn.GRU(self.d_model, self.d_model, batch_first=True)
        self.attention = ContextAttention(self.d_model, self.d_model, self.d_model)
        self.ffn = FeedForward(self.d_model, self.d_model)

    def forward(self, x, mask, hidden):

        lin_output = self.linear(x)

        att_output, att_weights = self.attention(lin_output, mask, hidden.transpose(0, 1))        

        rnn_output, rnn_hidden = self.rnn(att_output, hidden)

        ffn_output = self.ffn(rnn_output)

        return ffn_output, att_weights, rnn_hidden

    def initHidden(self, batch_size, device):
        return torch.zeros(1, batch_size, self.d_model).to(device)


class RNNDAEncoder(nn.Module):

    def __init__(self, da_size, emb_dim, d_model):
        super(RNNDAEncoder, self).__init__() 
        self.d_model = d_model
        self.embedding = DAEmbedding(da_size, emb_dim, self.d_model)
        self.rnn = nn.GRU(self.d_model, self.d_model, batch_first=True)
        self.attention = Attention(self.d_model)
        self.ffn = FeedForward(self.d_model, emb_dim)    

    def forward(self, X_da, mask, hidden):

        emb_output = self.embedding(X_da)
        
        _, rnn_hidden = self.rnn(emb_output, hidden)

        att_output, att_weights = self.attention(rnn_hidden.transpose(0, 1), rnn_hidden.transpose(0, 1), rnn_hidden.transpose(0, 1), mask)

        ffn_output = self.ffn(att_output)

        return ffn_output, att_weights, rnn_hidden

    def initHidden(self, batch_size, device):
        return torch.zeros(1, batch_size, self.d_model).to(device)


class RNNDAAwareEncoder(nn.Module):

    def __init__(self, da_size, emb_dim, d_model):
        super(RNNDAAwareEncoder, self).__init__()
        self.d_model = d_model
        self.embedding = DAEmbedding(da_size, emb_dim, self.d_model)
        self.rnn = nn.GRU(self.d_model, self.d_model, batch_first=True)
        self.attention = ContextAttention(self.d_model, self.d_model, self.d_model)
        self.ffn = FeedForward(self.d_model, emb_dim)

    def forward(self, X_da, mask, hidden):

        emb_output = self.embedding(X_da)

        att_output, att_weights = self.attention(emb_output, mask, hidden.transpose(0, 1))        

        rnn_output, rnn_hidden = self.rnn(att_output, hidden)

        ffn_output = self.ffn(rnn_output)

        return ffn_output, att_weights, rnn_hidden

    def initHidden(self, batch_size, device):
        return torch.zeros(1, batch_size, self.d_model).to(device)


class FeedForward(nn.Module):

    def __init__(self, d_model, d_ff, dropout=0.1):
        super(FeedForward, self).__init__()
        self.linear_1 = nn.Linear(d_model, d_ff)
        self.dropout = nn.Dropout(dropout)
        self.linear_2 = nn.Linear(d_ff, d_model)

    def forward(self, x):

        x = self.linear_1(x)

        x = self.dropout(F.relu(x))

        x = self.linear_2(x)

        return x


class Attention(nn.Module):

    def __init__(self, d_model):
        super(Attention, self).__init__()
        self.q_linear = nn.Linear(d_model, d_model)
        self.v_linear = nn.Linear(d_model, d_model)
        self.k_linear = nn.Linear(d_model, d_model)
        self.out = nn.Linear(d_model, d_model)
        self.d_k = d_model
       
    def forward(self, q, k, v, mask=None):
        # 全結合層で特徴量を変換
        k = self.k_linear(k)
        q = self.q_linear(q)
        v = self.v_linear(v)

        # Attentionの値を計算する
        # 各値を足し算すると大きくなりすぎるので、root(d_k)で割って調整
        weights = torch.matmul(q, k.transpose(1, 2)) / math.sqrt(self.d_k)
        
        # ここでmaskを計算
        if mask is not None:
            mask = mask.unsqueeze(1)
            weights = weights.masked_fill(mask == 0, -1e9)

        # softmaxで規格化をする
        attention_weights = F.softmax(weights, dim=-1)

        # AttentionをValueとかけ算
        output = torch.matmul(attention_weights, v)

        # 全結合層で特徴量を変換
        output = self.out(output)

        return output, attention_weights


class ContextAttention(nn.Module):

    def __init__(self, d_model, hidden_size, att_size):
        super(ContextAttention, self).__init__()
        self.q_linear = nn.Linear(att_size, att_size)
        self.v_linear = nn.Linear(att_size, att_size)
        self.k_linear = nn.Linear(att_size, att_size)

        self.fc_1 = nn.Linear(d_model, d_model)
        self.fc_3 = nn.Linear(hidden_size, d_model, bias=True)
        self.fc_2 = nn.Linear(d_model, att_size)

        self.fc_out = nn.Linear(att_size, hidden_size, bias=True)
        self.d_k = att_size

    def forward(self, x, mask, hidden):
        
        x = self.fc_2(torch.tanh(self.fc_1(x) + self.fc_3(hidden)))

        q = self.q_linear(x)
        v = self.v_linear(x)
        k = self.k_linear(x)

        weights = torch.matmul(q, k.transpose(1, 2)) / math.sqrt(self.d_k)

        # ここでmaskを計算
        if mask is not None:
            mask = mask.unsqueeze(1)
            weights = weights.masked_fill(mask == 0, -1e9)

        attention_weights = F.softmax(weights, dim=-1)

        att_output = torch.matmul(attention_weights, v)

        output = self.fc_out(att_output)

        return output, attention_weights


class PositinalEncoding(nn.Module):

    def __init__(self, d_model, max_len, dropout=0.1):
        super(PositinalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)
        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2).float() * (-math.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pe', pe)

    def forward(self, x):
        
        x = x + self.pe[:x.size(0), :]

        return self.dropout(x)


class WordEmbedding(nn.Module):

    def __init__(self, vocab_size, embed_size, w_model):
        super(WordEmbedding, self).__init__()
        self.word_embedding = nn.Embedding(vocab_size, embed_size)
        self.linear = nn.Linear(embed_size, w_model)

    def forward(self, x_word):
        return torch.tanh(self.linear(self.word_embedding(x_word)))


class DAEmbedding(nn.Module):

    def __init__(self, da_size, embed_size, d_model):
        super(DAEmbedding, self).__init__()
        self.da_embedding = nn.Embedding(da_size, embed_size)
        self.linear = nn.Linear(embed_size, d_model)

    def forward(self, x_da):
        return torch.tanh(self.linear(self.da_embedding(x_da)))


if __name__ == "__main__":
    
    # da_encoder = RNNDAEncoder(128, 256, 512)
    # context_encoder = RNNContextEncoder(256, 512)

    # a = torch.rand(128, 9, 513)
    # b = torch.randint(0, 100, (128, 9))
    # c = torch.randint(0, 1000, (128, 1, 513))

    x = torch.randint(0, 100, (128, 9))
    c = torch.randn((1, 128, 512))
    utter_rnn = OnlyRNNUtteraceEncoder(20000, 256, 512)
    x, h = utter_rnn(x,c)
    print(x.size(), h.size())


    # a = torch.rand(128, 1, 512)
    # h = torch.rand(1, 128, 512)
    # print(h.transpose(0, 1).size())
    # print((a+h.transpose(0, 1)).size())


    # c = torch.randn((1, 128, 256))
    # print(c.size())
    # print(c[0].view(128, -1).size())

    # rnn_hidden = torch.zeros(1, 128, 512)

    # x,y,z = context_encoder(a, None, rnn_hidden)
    # print(z.size())

import torch
import torch.nn as nn
from torch import optim
import time
import math
import numpy as np
import torch.nn.functional as F
from torchsummary import summary
import torch.nn.utils.rnn as rnn
from BlockModel import UtteraceEncoder, ContextEncoder, DAEncoder, DenceEncoder, RNNContextEncoder, RNNDAEncoder, OnlyRNNContextEncoder, OnlyRNNDAEncoder, OnlyRNNUtteraceEncoder


class HiAttentionOnlyModel(nn.Module):

    def __init__(self, utt_vocab, da_vocab, config, device):
        super(HiAttentionOnlyModel, self).__init__()
        self.utter_encoder = UtteraceEncoder(len(utt_vocab.word2id), config['UTT_EMBED'], config['UTT_HIDDEN'])

        self.context_encoder = ContextEncoder(config['CON_EMBED'], config['CON_HIDDEN'])

        self.da_encoder = DAEncoder(len(utt_vocab.word2id), config['DA_EMBED'], config['DA_HIDDEN'])

        self.de_encoder = DenceEncoder(config['DA_HIDDEN'] + config['CON_HIDDEN'], config['DA_EMBED'], len(da_vocab.word2id))

        self.cross_entropy_loss = nn.CrossEntropyLoss(size_average=True)

        self.device = device

    def forward(self, X_utter, X_da, Y_da, mask, context_hidden, da_hidden, turn):

        loss = 0
        ## 発話文の処理
        utter_output, utter_weights = self.utter_encoder(X_utter, mask)
        ## 発話ベクトルとユーザベクトルの連結
        turn_output = torch.cat((utter_output, turn), dim=2) # (128, 1, 512) + (128, 1, 1) = (128, 1, 513)
        ## 対話の処理
        context_output, context_hidden, context_weights = self.context_encoder(turn_output, mask, context_hidden) # (batch_size, 1, da_dim)
        ## 対話行為の処理
        da_output, da_hidden, da_weights = self.da_encoder(X_da, mask, da_hidden)
        ## 対話ベクトルと対話行為ベクトルの連結
        x_output = torch.cat((context_output, da_output), dim=2)  # (128, 1, 512) + (128, 1, 128)
        ## 連結ベクトルから予測
        dence_output = self.de_encoder(x_output)
        
        output = dence_output.squeeze(1)  # (batch_size, da_dim)
        
        Y_da = Y_da.squeeze()
        
        loss += self.cross_entropy_loss(output, Y_da)

        loss.backward(retain_graph=True)

        return loss.item(), context_hidden, da_hidden


    def evaluate(self, X_utter, X_da, Y_da, mask, context_hidden, da_hidden, turn):
        with torch.no_grad():
            loss = 0

            utter_output, utter_weights = self.utter_encoder(X_utter, mask)

            turn_output = torch.cat((utter_output, turn), dim=2)
        
            context_output, context_hidden, context_weights = self.context_encoder(turn_output, mask, context_hidden) # (batch_size, 1, da_dim)

            da_output, da_hidden, da_weights = self.da_encoder(X_da, mask, da_hidden)

            x_output = torch.cat((context_output, da_output), dim=2)

            dence_output = self.de_encoder(x_output)
            
            output = dence_output.squeeze(1)  # (batch_size, da_dim)
            
            Y_da = Y_da.squeeze(0)
            
            loss += self.cross_entropy_loss(output, Y_da)
        
        return loss, context_hidden, da_hidden


    def prediction(self, X_utter, X_da, mask, context_hidden, da_hidden, turn):

        with torch.no_grad():

            seq_len = X_utter.size()[1]
            for ei in range(seq_len):

                utter_output, utter_weights = self.utter_encoder(X_utter[ei], mask)

                turn_output = torch.cat((utter_output, turn), dim=2)

                context_output, context_hidden, context_weights = self.context_encoder(turn_output, mask, context_hidden)

                seq_len = context_weights.size()[1]
                con_weight = F.avg_pool2d(context_weights, (seq_len, 1))
                
                da_output, da_hidden, da_weights = self.da_encoder(X_da, mask, da_hidden)

                x_output = torch.cat((context_output, da_output), dim=2)

                dence_output = self.de_encoder(x_output)

                output = dence_output.squeeze(1)

        return output, context_hidden, da_hidden, con_weight.squeeze(1)


    def initDAHidden(self, batch_size):
        return None, self.context_encoder.initHidden(batch_size, self.device), self.da_encoder.initHidden(batch_size, self.device)


class HiRnnUtterAttentionModel(nn.Module):

    def __init__(self, utt_vocab, da_vocab, config, device):
        super(HiRnnUtterAttentionModel, self).__init__()
        self.utter_encoder = UtteraceEncoder(len(utt_vocab.word2id), config['UTT_EMBED'], config['UTT_HIDDEN'])

        self.context_encoder = OnlyRNNContextEncoder(config['CON_EMBED'], config['CON_HIDDEN'])

        self.da_encoder = OnlyRNNDAEncoder(len(utt_vocab.word2id), config['DA_EMBED'], config['DA_HIDDEN'])

        self.de_encoder = DenceEncoder(config['DA_HIDDEN'] + config['CON_HIDDEN'], config['DA_EMBED'], len(da_vocab.word2id))

        self.cross_entropy_loss = nn.CrossEntropyLoss()

        self.device = device

    def forward(self, X_utter, X_da, Y_da, mask, utter_hidden, context_hidden, da_hidden, turn):
        
        utter_output, utter_weights = self.utter_encoder(X_utter, mask)

        turn_output = torch.cat((utter_output, turn), dim=2)

        context_output, context_hidden = self.context_encoder(turn_output, context_hidden)

        da_output, da_hidden = self.da_encoder(X_da, da_hidden)

        x_output = torch.cat((context_output, da_output), dim=2)

        dence_output = self.de_encoder(x_output)

        output = dence_output.squeeze(1)  # (batch_size, da_dim)
        
        Y_da = Y_da.squeeze()
        
        loss = self.cross_entropy_loss(output, Y_da)

        loss.backward(retain_graph=True)

        return loss.item(), utter_hidden, context_hidden, da_hidden 

    def evaluate(self, X_utter, X_da, Y_da, mask, utter_hidden, context_hidden, da_hidden, turn):
        with torch.no_grad():

            utter_output, utter_weights = self.utter_encoder(X_utter, mask)

            turn_output = torch.cat((utter_output, turn), dim=2)

            context_output, context_hidden = self.context_encoder(turn_output, context_hidden)

            da_output, da_hidden = self.da_encoder(X_da, da_hidden)

            x_output = torch.cat((context_output, da_output), dim=2)

            dence_output = self.de_encoder(x_output)

            output = dence_output.squeeze(1)  
            
            Y_da = Y_da.squeeze(0)
        
            loss = self.cross_entropy_loss(output, Y_da)

        return loss.item(), utter_hidden, context_hidden, da_hidden

    def prediction(self, X_utter, X_da, mask, utter_hidden, context_hidden, da_hidden, turn):
        with torch.no_grad():

            utter_output, utter_weights = self.utter_encoder(X_utter, mask)

            turn_output = torch.cat((utter_output, turn), dim=2)

            context_output, context_hidden = self.context_encoder(turn_output, context_hidden)

            da_output, da_hidden = self.da_encoder(X_da, da_hidden)

            x_output = torch.cat((context_output, da_output), dim=2)

            dence_output = self.de_encoder(x_output)

            output = dence_output.squeeze(1)  

        return load.item(), utter_hidden, context_hidden, da_hidden 


    def initDAHidden(self, batch_size):
        return None, self.context_encoder.initHidden(batch_size, self.device), self.da_encoder.initHidden(batch_size, self.device)


class HiRnnContextAttentionModel(nn.Module):
    def __init__(self, utt_vocab, da_vocab, config, device):
        super(HiRnnContextAttentionModel, self).__init__()
        self.utter_encoder = OnlyRNNUtteraceEncoder(len(utt_vocab.word2id), config['UTT_EMBED'], config['UTT_HIDDEN'])

        self.context_encoder = ContextEncoder(config['CON_EMBED'], config['CON_HIDDEN'])

        self.da_encoder = DAEncoder(len(utt_vocab.word2id), config['DA_EMBED'], config['DA_HIDDEN'])

        self.de_encoder = DenceEncoder(config['DA_HIDDEN'] + config['CON_HIDDEN'], config['DA_EMBED'], len(da_vocab.word2id))

        self.cross_entropy_loss = nn.CrossEntropyLoss()

        self.device = device

    def forward(self, X_utter, X_da, Y_da, mask, utter_hidden, context_hidden, da_hidden, turn):

        utter_output, utter_hidden = self.utter_encoder(X_utter, utter_hidden)

        turn_output = torch.cat((utter_output, turn), dim=2)

        context_output, context_hidden, context_weights = self.context_encoder(turn_output, mask, context_hidden) 
        
        da_output, da_hidden, da_weights = self.da_encoder(X_da, mask, da_hidden)
        
        x_output = torch.cat((context_output, da_output), dim=2)  
        
        dence_output = self.de_encoder(x_output)
        
        output = dence_output.squeeze(1) 
        
        Y_da = Y_da.squeeze()
        
        loss = self.cross_entropy_loss(output, Y_da)

        loss.backward(retain_graph=True)

        return loss.item(), utter_hidden, context_hidden, da_hidden

    
    def evaluate(self, X_utter, X_da, Y_da, mask, utter_hidden, context_hidden, da_hidden, turn):
        with torch.no_grad():

            utter_output, utter_hidden = self.utter_encoder(X_utter, utter_hidden)

            turn_output = torch.cat((utter_output, turn), dim=2)

            context_output, context_hidden, context_weights = self.context_encoder(turn_output, mask, context_hidden)

            da_output, da_hidden, da_weights = self.da_encoder(X_da, mask, da_hidden)
        
            x_output = torch.cat((context_output, da_output), dim=2)  
            
            dence_output = self.de_encoder(x_output)
            
            output = dence_output.squeeze(1) 
            
            Y_da = Y_da.squeeze(0)
            
            loss = self.cross_entropy_loss(output, Y_da)

        return loss.item(), utter_hidden, context_hidden, da_hidden

    def prediction(self, X_utter, X_da, mask, utter_hidden, context_hidden, da_hidden, turn):
        with torch.no_grad():

            utter_output, utter_hidden = self.utter_encoder(X_utter, utter_hidden)

            turn_output = torch.cat((utter_output, turn), dim=2)

            context_output, context_hidden, context_weights = self.context_encoder(turn_output, mask, context_hidden)

            da_output, da_hidden, da_weights = self.da_encoder(X_da, mask, da_hidden)
        
            x_output = torch.cat((context_output, da_output), dim=2)  
            
            dence_output = self.de_encoder(x_output)
            
            output = dence_output.squeeze(1) 

        return output, Noneutter_hidden, context_hidden, da_hidden


    def initDAHidden(self, batch_size):
        return None, self.context_encoder.initHidden(batch_size, self.device), self.da_encoder.initHidden(batch_size, self.device)


class NewDAPredictModel(nn.Module):

    def __init__(self, utt_vocab, da_vocab, config, device):
        super(NewDAPredictModel, self).__init__()
        self.utter_encoder = UtteraceEncoder(len(utt_vocab.word2id), config['UTT_EMBED'], config['UTT_HIDDEN'])

        self.context_encoder = RNNContextEncoder(config['CON_EMBED'], config['CON_HIDDEN'])

        self.da_encoder = RNNDAEncoder(len(utt_vocab.word2id), config['DA_EMBED'], config['DA_HIDDEN'])

        self.de_encoder = DenceEncoder(config['DA_HIDDEN'] + config['CON_HIDDEN'], config['DA_EMBED'], len(da_vocab.word2id))

        self.cross_entropy_loss = nn.CrossEntropyLoss()

        self.device = device

    def forward(self, X_utter, X_da, Y_da, mask, context_hidden, da_hidden, turn):

        loss = 0

        utter_output, utter_weights = self.utter_encoder(X_utter, mask)

        turn_output = torch.cat((utter_output, turn), dim=2)

        context_output, context_weights, context_hidden = self.context_encoder(turn_output, mask, context_hidden)

        da_output, da_weights, da_hidden = self.da_encoder(X_da, mask, da_hidden)

        x_output = torch.cat((context_output, da_output), dim=2)

        dence_output = self.de_encoder(x_output)

        output = dence_output.squeeze(1)  # (batch_size, da_dim)
        
        Y_da = Y_da.squeeze()
        
        loss += self.cross_entropy_loss(output, Y_da)

        loss.backward(retain_graph=True)

        return loss.item(), context_hidden, da_hidden 

    def evaluate(self, X_utter, X_da, Y_da, mask, context_hidden, da_hidden, turn):
        with torch.no_grad():

            loss = 0

            utter_output, utter_weights = self.utter_encoder(X_utter, mask)

            turn_output = torch.cat((utter_output, turn), dim=2)

            context_output, context_weights, context_hidden = self.context_encoder(turn_output, mask, context_hidden)

            da_output, da_weights, da_hidden = self.da_encoder(X_da, mask, da_hidden)

            x_output = torch.cat((context_output, da_output), dim=2)

            dence_output = self.de_encoder(x_output)

            output = dence_output.squeeze(1)  # (batch_size, da_dim)
            
            Y_da = Y_da.squeeze(0)
            
            loss += self.cross_entropy_loss(output, Y_da)

        return loss.item(), context_hidden, da_hidden

    def prediction(self, X_utter, X_da, mask, context_hidden, da_hidden, turn):
        with torch.no_grad():
            seq_len = X_utter.size()[1]
            for ei in range(seq_len):
                loss = 0

                utter_output, utter_weights = self.utter_encoder(X_utter[ei], mask)

                turn_output = torch.cat((utter_output, turn), dim=2)

                context_output, context_weights, context_hidden = self.context_encoder(turn_output, mask, context_hidden)

                seq_len = context_weights.size()[1]
                con_weight = F.avg_pool2d(context_weights, (seq_len, 1))

                da_output, da_weights, da_hidden = self.da_encoder(X_da, mask, da_hidden)

                x_output = torch.cat((context_output, da_output), dim=2)

                dence_output = self.de_encoder(x_output)

                output = dence_output.squeeze(1)

        return output, context_hidden, da_hidden, con_weight.squeeze(1)


    def initDAHidden(self, batch_size):
        return self.context_encoder.initHidden(batch_size, self.device), self.da_encoder.initHidden(batch_size, self.device)


class RNNDAPredictionModel(nn.Module):
    
    def __init__(self, utt_vocab, da_vocab, config, device):
        super(RNNDAPredictionModel, self).__init__()
        self.utter_encoder = UtteraceEncoder(len(utt_vocab.word2id), config['UTT_EMBED'], config['UTT_HIDDEN'])

        self.context_encoder = OnlyRNNContextEncoder(config['CON_EMBED'], config['CON_HIDDEN'])

        self.da_encoder = OnlyRNNDAEncoder(len(utt_vocab.word2id), config['DA_EMBED'], config['DA_HIDDEN'])

        self.de_encoder = DenceEncoder(config['DA_HIDDEN'] + config['CON_HIDDEN'], config['DA_EMBED'], len(da_vocab.word2id))

        self.cross_entropy_loss = nn.CrossEntropyLoss()

        self.device = device

    def forward(self, X_utter, X_da, Y_da, mask, context_hidden, da_hidden, turn):

        loss = 0
        ## 発話文の処理
        utter_output, utter_weights = self.utter_encoder(X_utter, mask)
        ## 発話ベクトルとユーザベクトルの連結
        turn_output = torch.cat((utter_output, turn), dim=2) # (128, 1, 512) + (128, 1, 1) = (128, 1, 513)
        ## 対話の処理
        context_output, context_hidden = self.context_encoder(turn_output, mask, context_hidden) # (batch_size, 1, da_dim)
        ## 対話行為の処理
        da_output, da_hidden = self.da_encoder(X_da, mask, da_hidden)
        ## 対話ベクトルと対話行為ベクトルの連結
        x_output = torch.cat((context_output, da_output), dim=2)  # (128, 1, 512) + (128, 1, 128)
        ## 連結ベクトルから予測
        dence_output = self.de_encoder(x_output)
        
        output = dence_output.squeeze(1)  # (batch_size, da_dim)
        
        Y_da = Y_da.squeeze()
        
        loss += self.cross_entropy_loss(output, Y_da)

        loss.backward(retain_graph=True)

        return loss.item(), context_hidden, da_hidden

    def evaluate(self, X_utter, X_da, Y_da, mask, context_hidden, da_hidden, turn):
        with torch.no_grad():
            loss = 0

            utter_output, utter_weights = self.utter_encoder(X_utter, mask)

            turn_output = torch.cat((utter_output, turn), dim=2)
        
            context_output, context_hidden = self.context_encoder(turn_output, mask, context_hidden) # (batch_size, 1, da_dim)

            da_output, da_hidden = self.da_encoder(X_da, mask, da_hidden)

            x_output = torch.cat((context_output, da_output), dim=2)

            dence_output = self.de_encoder(x_output)
            
            output = dence_output.squeeze(1)  # (batch_size, da_dim)
            
            Y_da = Y_da.squeeze(0)
            
            loss += self.cross_entropy_loss(output, Y_da)
        
        return loss, context_hidden, da_hidden

    def prediction(self, X_utter, X_da, mask, context_hidden, da_hidden, turn):

        with torch.no_grad():

            seq_len = X_utter.size()[1]
            for ei in range(seq_len):

                utter_output, utter_weights = self.utter_encoder(X_utter[ei], mask)

                turn_output = torch.cat((utter_output, turn), dim=2)

                context_output, context_hidden = self.context_encoder(turn_output, mask, context_hidden)
                
                da_output, da_hidden = self.da_encoder(X_da, mask, da_hidden)

                x_output = torch.cat((context_output, da_output), dim=2)

                dence_output = self.de_encoder(x_output)

                output = dence_output.squeeze(1)

        return output, context_hidden, da_hidden, None


    def initDAHidden(self, batch_size):
        return self.context_encoder.initHidden(batch_size, self.device), self.da_encoder.initHidden(batch_size, self.device)


class DAUtterOnlyModel(nn.Module):

    def __init__(self, utt_vocab, config, device):
        super(DAUtterOnlyModel, self).__init__()
        self.utter_encoder = UtteraceEncoder(len(utt_vocab.word2id), config['UTT_EMBED'], config['UTT_HIDDEN'])
        self.context_encoder = ContextEncoder(config['CON_EMBED'], config['CON_HIDDEN'])
        self.de_encoder = DenceEncoder(config['CON_HIDDEN'], config['DA_EMBED'], len(da_vocab.word2id))
        self.cross_entropy_loss = nn.CrossEntropyLoss()
        self.device = device

    def forward(self, X_utter, Y_da, mask, context_hidden, turn):

        loss = 0
        ## 発話文の処理
        utter_output = self.utter_encoder(X_utter, mask)
        ## 発話ベクトルとユーザベクトルの連結
        turn_output = torch.cat((utter_output, turn), dim=2) # (128, 1, 512) + (128, 1, 1) = (128, 1, 513)
        ## 対話の処理
        context_output, context_hidden = self.context_encoder(turn_output, mask, context_hidden) # (batch_size, 1, da_dim)
        ## 連結ベクトルから予測
        dence_output = self.de_encoder(context_output)
        
        output = dence_output.squeeze(1)  # (batch_size, da_dim)
        
        Y_da = Y_da.squeeze()
        
        loss += self.cross_entropy_loss(output, Y_da)

        loss.backward(retain_graph=True)

        return loss.item(), context_hidden

    def validate(self, X_utter, Y_da, mask, context_hidden, turn):

        pass

    def prediction(self, X_utter, mask, context_hidden, turn):
    
        pass

if __name__ == "__main__":
    a = torch.randint(0, 1000, (128, 100))
    b = torch.randint(0, 1000, (128, 100))
    c = torch.randint(0, 1000, (128, 100))
    d = torch.randint(0, 1000, (128, 100))
    e = torch.randint(0, 1000, (128, 100))
    l = [a, b, c, d, e]
    s = torch.randint(0, 100, (1,2,3))
    seq_len = s.size()[1]
    for i in range(seq_len):
        print(s[i].size())
    da_pred = Encoder(20000, 256, 512, 10)
    for i, v in enumerate(l):
        context_output = da_pred(v, None, i+1)
        print(context_output.size())

    # s = torch.randint(0, 1000, (128, 200, 256))
    # len_seq = len(s[0])
    # print(len_seq)